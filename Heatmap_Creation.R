install.packages("gplots")
library(gplots)
top500 <- head(cuffData[order(rowSums(cuffData[,2:6]),decreasing = T),2:6],500)
top500
ramp <- colorRampPalette(c("chartreuse3","black","firebrick"))(100)
colbreaks <- 
heatmap.2(log10(as.matrix(cuffData)),trace = 'none',margins = c(8,8),col = ramp,
          key.title = "expression values",key.xlab = "log10(FPKM+1)",main = "expression heatmap")


Apoptosis_Pro_apoptosis_Cell_death <- c("Bim","Bad","Bik","Bax","Casp1","Casp3","Casp8","IL2RA","TSC22D3","ADA","CCL5","NFKBIA","TIMP1","TGFB1","PPIF","NFKB1","IL12B","TNFAIP3","INHBA","IRF1","TNFSF9","CCR7","PYCARD","IDO1","RARA","SORT1","IL2RA","DRAM1","IL6","RNF144B","THBS1","IL1A","IL1B","BIRC3","BCL2A1","BMP6","NCF1","TNFRSF21","CSF2","SERPINB2","DDIT4","CCL19","P2RX7","F3","P2RX4","SOD2","LITAF","ALDH1A2","G0S2","PTGS2","VNN1","MNDA","GADD45A","CD40","TSC22D3","ADA","ZC3H12A","KLF4","MAP3K8","CARD16","TNFRSF4","TNFRSF18","ALOX15B","SERPINB9","ANXA4","STK26","NBN","DFNA5","IL10","CFLAR","CD44","SPN","YWHAZ","ARHGEF2","GSN","ADORA2A","PLAC8","CTSC","DHCR24","TRAF1","EPB41L3","RASSF5","SOCS3","CCL5","CCL3")
Anti_Apoptosis_Cell_Survival_Proliferation <- c("AKT1", "BIRC5", "BCL2", "BCL2I1", "BCL-XL","FOXO1", "CYP27B1","EGLN3","OCSTAMP","FGR","CD274","NCF1","GLUL","JAK1","IL1B","TNFRSF4","RBPJ","SOD2","NR4A1","WARS","BNIP2-011")
Tracription_Factors <- c("FOXO1", "FOXO3", "FOXP3", "STAT1", "STAT3","STAT5B", "ZNF366", "CD40", "STAT4", "MXI1", "IRF1", "IRF8")
Inflammatory_Response <- c("CCL26","TNIP3","TGFB1","NFKB1","IL23A","IL12B","TNFAIP3","TNFAIP6","C3","CCR7","PYCARD","IDO1","FABP4",'IRAK2',"IL2RA","IL6","THBS1","IL1A","IL1B","BIRC3","LXN","IGFBP4","BMP6","CSF1R","NCF1","ALOX15","CXCL3","CXCL5","CXCL8","CCL19","TNIP1","NFKBIZ","P2RX7","F3","LIPA","CSF1","CCL20","PTGS2","VNN1","NFKB2","CD40","ADA","KLF4","TNFRSF4","CXCL1","IL10","CD44","SPN","YWHAZ","CFB","ADORA2A","CEBPA","CXCL2","RBPJ","OLR1","ITGAM","SOCS3","CCL5","CCL3","CCL4","CCL3L3","CCL4L1", "Casp1", "Casp3", "Casp8", "IL-1b", "IL-6", "IL-12b")
Immunoregulation_Immunosuppression <- c("IDO1", "IDO2", "FOXP3", "CD80", "PD-L1","CD274", "IL10")

GeneID_Apop <- subset(humanData,humanData$external_gene_name %in% Apoptosis_Pro_apoptosis_Cell_death)
GeneID_Apop <- GeneID_Apop[,1]
GeneID_Anti_Apop <- subset(humanData,humanData$external_gene_name %in% Anti_Apoptosis_Cell_Survival_Proliferation)
GeneID_Anti_Apop <- GeneID_Anti_Apop[,1]
GeneID_TF <- subset(humanData,humanData$external_gene_name %in% Tracription_Factors)
GeneID_TF <- GeneID_TF[,1]
GeneID_Inflam <- subset(humanData,humanData$external_gene_name %in% Inflammatory_Response)
GeneID_Inflam <- GeneID_Inflam[,1]
GeneID_Immune <- subset(humanData,humanData$external_gene_name %in% Immunoregulation_Immunosuppression)
GeneID_Immune <- GeneID_Immune[,1]

diffData <- cuffData[,grep("de",colnames(cuffData))]
diffData[diffData==0] <- NA
logical <- apply(diffData,1, function(x) all(is.na(x)))
sig <- cuffData[!logical,7:10]

sig <- na.omit(sig)
logical <- apply(sig,1, function(x) all(is.finite(x)))
sig <- sig[logical,]
mat <- as.matrix(sig)
colnames(mat) <- c("PG381","DPG3","MFI", "MFB")

test <- mat[1:3000,]
dim(test)
colors = c(seq(-10,-5,length=20),seq(-4.9,5,length=100),seq(5.1,10,length=20))


my_palette <- colorRampPalette(c("green","black","red"))(n = 139)

heatmap.2(test, col=my_palette, trace="none",
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="log2(foldchange)", key.xlab="value",
          main="Differential expression", margins=c(8,8)
)


dist_pg <- dist(1-cor(t(mat)))
dist_test <- dist(1-cor(t(test)))
dist_test_2 <- dist(test)
dist_mat <- dist(mat)

ind <- apply(mat, 1, var) == 0
new_mat <- mat[!ind,]


dist_clus <- hclust(as.dist(1-cor(t(new_mat))),method = "complete",members = NULL)

cluster_mat <- hclust(dist_mat,method = "complete",members = NULL)
cluster_test <- hclust(dist_test,method = "complete",members = NULL)
plot(cluster_test)

cluster_test$height

cut_dist <- cutree(dist_clus,k=2000)
cut_mat <- cutree(cluster_mat,k=10000)
cut_test <- cutree(cluster_test,k=1000)
cut_test

cluster_2 <- new_mat[cut_dist==1,]
cluster_1_mat <- mat[cut_mat ==1,]
cluster_1 <- test[cut_test ==1,]
cluster_1_2 <- test[cut_test ==1,]
clus <- cut_test == 1
clus

colors = c(seq(-10,-4,length=5),seq(-3.9,4,length=10),seq(4.1,10,length=5))
colors = c(seq(-5,0,length=20),seq(0.1,5,length=20))

rows <- nrow(cluster_2)
rows
col <- ncol(cluster_2)
col
my_palette <- colorRampPalette(c("green","grey18","red"))(n = 19)
my_palette <- colorRampPalette(c("green","red"))(n = 39)

pdf("testCluster.pdf", width=4.5, height=7.23)
heatmap.2(cluster_2, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,1,5.7,2)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.5),lhei = c(.5,3.2,.98,.37),
          main="Differential expression",cexCol = 1.1,margins = c(4,9)
)
dev.off()

#Expressed genes

diffData_2 <- cuffData[,grep("de",colnames(cuffData))]
diffData_2 <- diffData_2[,1:4]
diffData_2[diffData_2==-1]<-1
diffData_2 <- diffData_2[rowSums(diffData_2)>0,]
colnames(diffData_2) <- c("pg381","dpg3","mfi","mfb")
venn(diffData_2)

#Selecting fold chnage values for expressed genes
PG381_sig_genes <- diffData_2[diffData_2$pg381==1 & diffData_2$dpg3==0 & diffData_2$mfi==0 & diffData_2$mfb==0,]
PG381_sig_genes <- subset(PG381_sig_genes,select = pg381)
PG381_fold <- subset(cuffData,select = pg381.fc)
colnames(PG381_fold) <- c("pg381")
PG381_fold_sig_gene <- merge(PG381_sig_genes,PG381_fold,by="row.names",all.x = T)
PG381_fold_sig_gene <- subset(PG381_fold_sig_gene,select = pg381.y)
genes <- PG381_fold_sig_genes$Row.names
row.names(PG381_fold_sig_gene) <- genes

logical <- apply(PG381_fold_sig_gene,1, function(x) all(is.finite(x)))
PG381_fold_sig_gene <- PG381_fold_sig_gene[logical,]
pg.max <- as.matrix(PG381_fold_sig_gene)
colnames(pg.max) <- c("PG381")

heatmap.2(PG381_fold_sig_gene, col=my_palette, trace="none",
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="log2(foldchange)", key.xlab="value",
          main="Differential expression", margins=c(8,8)
)


test_cuff <- cuffData

#Wild Type fold change
fc <- subset(cuffData,select = wt)
names <- row.names(fc)
fc <- log2(fc$wt) 
fc <- (0-fc)
test_cuff$wt.fc <- fc
test_cuff <- test_cuff[,c(1,2,3,4,5,6,22,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21)]

testData <- test_cuff[,grep("de",colnames(test_cuff))]
testData[testData==0] <- NA
logical <- apply(testData,1, function(x) all(is.na(x)))
testsig <- test_cuff[!logical,9:13]

testsig <- na.omit(testsig)
logical <- apply(testsig,1, function(x) all(is.finite(x)))
testsig <- testsig[logical,]
testmat <- as.matrix(testsig)
colnames(testmat) <- c("WT","PG381","DPG3","MFI", "MFB")

colors = c(seq(-10,-5,length=6),seq(-4.9,5,length=20),seq(5.1,10,length=6))
colors = c(seq(-5,0,length=5),seq(0.1,5,length=5))
my_palette <- colorRampPalette(c("green","black","red"))(n = 31)
my_palette <- colorRampPalette(c("green","red"))(n = 9)

tt <- testmat[1:1000,]
heatmap.2(tt, col=my_palette, trace="none",revC = T,#Colv = F,
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="log2(foldchange)", key.xlab="value",
          main="Differential expression", margins=c(8,8)
)

heatmap.2(test, col=my_palette, trace="none",
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="log2(foldchange)", key.xlab="value",
          main="Differential expression", margins=c(8,8)
)

sum(is.infinite(mat))
sum(is.na(mat))
sum(is.nan(mat))

#GO analysis
genes <- unique(as.character(humanData[humanData$ensembl_transcript_id %in% cuffData$id,]$ensembl_gene_id))

GO <- read.table("commonGenes.go.csv",sep= ",")
Anti_Apop <- GO[grep("anti-apoptosis",GO$V3),]
diffData <- cuffData[,grep("de",colnames(cuffData))]

#Anti-apop -> GO:0043066
#Immuno response -> GO:0006955
#Cell Survival -> 

Anti_Apop <- subset(humanGOdata,humanGOdata$go_id=="GO:0043066")
Anti_Apop <- Anti_Apop[,1]


cuff_Apop <- subset(cuffData,cuffData$id %in% GeneID_Apop)
cuff_Anti_Apop <- subset(cuffData,cuffData$id %in% GeneID_Anti_Apop)
cuff_TF <- subset(cuffData,cuffData$id %in% GeneID_TF)
cuff_Immune <- subset(cuffData,cuffData$id %in% GeneID_Immune) 
cuff_Inflam <- subset(cuffData,cuffData$id %in% GeneID_Inflam)


diff_anti_apop <- cuff_Anti_Apop[,grep("de",colnames(cuff_Anti_Apop))]
diff_anti_apop[diff_apop==0] <- NA
logical <- apply(diff_anti_apop,1, function(x) all(is.na(x)))
sig_anti_apop <- cuff_Anti_Apop[!logical,7:10]

sig_anti_apop <- na.omit(sig_anti_apop)
logical <- apply(sig_anti_apop,1, function(x) all(is.finite(x)))
sig_anti_apop <- sig_anti_apop[logical,]
mat_anti_apop <- as.matrix(sig_anti_apop)
colnames(mat_anti_apop) <- c("PG381","DPG3","MFI", "MFB")

dist_apop <- hclust(as.dist(1-cor(t(mat_apop))),method = "complete",members = NULL)
cut_apop <- cutree(dist_apop,k=20)
cluster_apop <- mat_apop[cut_apop==1,]

colors = c(seq(-10,-3,length=5),seq(-2.9,3,length=10),seq(3.1,10,length=5))
my_palette <- colorRampPalette(c("green","grey18","red"))(n = 19)
rows <- nrow(mat_apop)
col <- ncol(mat_apop)


pdf("testFunctionalCluster.pdf", width=4.5, height=7.5)
heatmap.2(mat_anti_apop, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,1,5.7,2)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,3.4,.98,.37),
          main="Differential expression \n Anti Apoptosis/Cell Survival \n Proliferation",cexCol = 1.1,margins = c(4,9)
)
dev.off()

#Clusters gene list

A <- heatmap.2(mat_Apop)
AA <- heatmap.2(mat_Anti_Apop)
TF <- heatmap.2(mat_TF)
Immune <- heatmap.2(mat_Immune)
Inflam <- heatmap.2(mat_Inflam)

Apop_gene_list <- rownames(mat_Apop[rev(A$rowInd),A$colInd]) 
Apop_gene_list
Anti_Apop_gene_list <- rownames(mat_Anti_Apop[rev(AA$rowInd),AA$colInd])
Anti_Apop_gene_list
TF_gene_list <- rownames(mat_TF[rev(TF$rowInd),TF$colInd])
TF_gene_list
Immune_gene_list <- rownames(mat_Immune[rev(Immune$rowInd),Immune$colInd])
Inflam_gene_list <- rownames(mat_Inflam[rev(Inflam$rowInd),Inflam$colInd])


##Fold Change for analysis

fc <- subset(cuffData,select = wt)
names <- row.names(fc)
fc <- log2(fc$wt) 
fc <- (0-fc)
test_cuff$wt.fc <- fc
test_cuff <- test_cuff[,c(1,2,3,4,5,6,22,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21)]

testcuff <- cuffData
testcuff.fc <- log2(testcuff[,2:6])
colnames(testcuff.fc) <- c("wt.f2c","pg381.f2c","dpg3.f2c","mfi.f2c","mfb.f2c")
testcuff$wt.f2c <- testcuff.fc$wt.f2c
testcuff$pg381.f2c <- testcuff.fc$pg381.f2c
testcuff$dpg.f2c <- testcuff.fc$dpg3.f2c
testcuff$mfi.f2c <- testcuff.fc$mfi.f2c
testcuff$mfb.f2c <- testcuff.fc$mfb.f2c


##Final Code
install.packages("gplots")
library(gplots)
load("~/Desktop/Pachiappan/cuffData.rda")
load("~/Desktop/Pachiappan/humanData.rda")

Apoptosis_Pro_apoptosis_Cell_death <- c("Bim","Bad","Bik","Bax","Casp1","Casp3","Casp8","IL2RA","TSC22D3","ADA","CCL5","NFKBIA","TIMP1","TGFB1","PPIF","NFKB1","IL12B","TNFAIP3","INHBA","IRF1","TNFSF9","CCR7","PYCARD","IDO1","RARA","SORT1","IL2RA","DRAM1","IL6","RNF144B","THBS1","IL1A","IL1B","BIRC3","BCL2A1","BMP6","NCF1","TNFRSF21","CSF2","SERPINB2","DDIT4","CCL19","P2RX7","F3","P2RX4","SOD2","LITAF","ALDH1A2","G0S2","PTGS2","VNN1","MNDA","GADD45A","CD40","TSC22D3","ADA","ZC3H12A","KLF4","MAP3K8","CARD16","TNFRSF4","TNFRSF18","ALOX15B","SERPINB9","ANXA4","STK26","NBN","DFNA5","IL10","CFLAR","CD44","SPN","YWHAZ","ARHGEF2","GSN","ADORA2A","PLAC8","CTSC","DHCR24","TRAF1","EPB41L3","RASSF5","SOCS3","CCL5","CCL3")
Anti_Apoptosis_Cell_Survival_Proliferation <- c("AKT1", "BIRC5", "BCL2", "BCL2I1", "BCL-XL","FOXO1", "CYP27B1","EGLN3","OCSTAMP","FGR","CD274","NCF1","GLUL","JAK1","IL1B","TNFRSF4","RBPJ","SOD2","NR4A1","WARS","BNIP2-011")
Tracription_Factors <- c("FOXO1", "FOXO3", "FOXP3", "STAT1", "STAT3","STAT5B", "ZNF366", "CD40", "STAT4", "MXI1", "IRF1", "IRF8")
Inflammatory_Response <- c("CCL26","TNIP3","TGFB1","NFKB1","IL23A","IL12B","TNFAIP3","TNFAIP6","C3","CCR7","PYCARD","IDO1","FABP4",'IRAK2',"IL2RA","IL6","THBS1","IL1A","IL1B","BIRC3","LXN","IGFBP4","BMP6","CSF1R","NCF1","ALOX15","CXCL3","CXCL5","CXCL8","CCL19","TNIP1","NFKBIZ","P2RX7","F3","LIPA","CSF1","CCL20","PTGS2","VNN1","NFKB2","CD40","ADA","KLF4","TNFRSF4","CXCL1","IL10","CD44","SPN","YWHAZ","CFB","ADORA2A","CEBPA","CXCL2","RBPJ","OLR1","ITGAM","SOCS3","CCL5","CCL3","CCL4","CCL3L3","CCL4L1", "Casp1", "Casp3", "Casp8", "IL-1b", "IL-6", "IL-12b")
Immunoregulation_Immunosuppression <- c("IDO1", "IDO2", "FOXP3", "CD80", "PD-L1","CD274", "IL10")

GeneID_Apop <- subset(humanData,humanData$external_gene_name %in% Apoptosis_Pro_apoptosis_Cell_death)
GeneID_Apop <- GeneID_Apop[,1]
GeneID_Anti_Apop <- subset(humanData,humanData$external_gene_name %in% Anti_Apoptosis_Cell_Survival_Proliferation)
GeneID_Anti_Apop <- GeneID_Anti_Apop[,1]
GeneID_TF <- subset(humanData,humanData$external_gene_name %in% Tracription_Factors)
GeneID_TF <- GeneID_TF[,1]
GeneID_Inflam <- subset(humanData,humanData$external_gene_name %in% Inflammatory_Response)
GeneID_Inflam <- GeneID_Inflam[,1]
GeneID_Immune <- subset(humanData,humanData$external_gene_name %in% Immunoregulation_Immunosuppression)
GeneID_Immune <- GeneID_Immune[,1]

cuff_Apop <- subset(cuffData,cuffData$id %in% GeneID_Apop)
cuff_Anti_Apop <- subset(cuffData,cuffData$id %in% GeneID_Anti_Apop)
cuff_TF <- subset(cuffData,cuffData$id %in% GeneID_TF)
cuff_Immune <- subset(cuffData,cuffData$id %in% GeneID_Immune) 
cuff_Inflam <- subset(cuffData,cuffData$id %in% GeneID_Inflam)

diff_Apop <- cuff_Apop[,grep("de",colnames(cuff_Apop))]
diff_Anti_Apop <- cuff_Anti_Apop[,grep("de",colnames(cuff_Anti_Apop))]
diff_TF <- cuff_TF[,grep("de",colnames(cuff_TF))]
diff_Immune <- cuff_Immune[,grep("de",colnames(cuff_Immune))]
diff_Inflam <- cuff_Inflam[,grep("de",colnames(cuff_Inflam))]

diff_Apop[diff_Apop==0] <- NA
diff_Anti_Apop[diff_Anti_Apop==0] <- NA
diff_TF[diff_TF==0] <- NA
diff_Immune[diff_Immune==0] <- NA
diff_Inflam[diff_Inflam==0] <- NA

logical_1 <- apply(diff_Apop,1, function(x) all(is.na(x)))
logical_2 <- apply(diff_Anti_Apop,1, function(x) all(is.na(x)))
logical_3 <- apply(diff_TF,1, function(x) all(is.na(x)))
logical_4 <- apply(diff_Immune,1, function(x) all(is.na(x)))
logical_5 <- apply(diff_Inflam,1, function(x) all(is.na(x)))

sig_Apop <- cuff_Apop[!logical_1,7:10]#######################HERE
sig_Apop_FPKM <- cuff_Apop[!logical_1,2:3]
sig_Anti_Apop <- cuff_Anti_Apop[!logical_2,7:10]
sig_TF <- cuff_TF[!logical_3,7:10]
sig_Immune <- cuff_Immune[!logical_4,7:10]
sig_Inflam <- cuff_Inflam[!logical_5,7:10]

sig_Apop <- na.omit(sig_Apop)
sig_Apop_FPKM <- na.omit(sig_Apop_FPKM)
sig_Anti_Apop <- na.omit(sig_Anti_Apop)
sig_TF <- na.omit(sig_TF)
sig_Immune <- na.omit(sig_Immune)
sig_Inflam <- na.omit(sig_Inflam)

sig_Apop_FPKM <- log2(sig_Apop_FPKM)

logical_1 <- apply(sig_Apop,1, function(x) all(is.finite(x)))
logical_1.5 <- apply(sig_Apop_FPKM,1,function(x) all(is.finite(x)))
logical_2 <- apply(sig_Anti_Apop,1, function(x) all(is.finite(x)))
logical_3 <- apply(sig_TF,1, function(x) all(is.finite(x)))
logical_4 <- apply(sig_Immune,1, function(x) all(is.finite(x)))
logical_5 <- apply(sig_Inflam,1, function(x) all(is.finite(x)))

sig_Apop <- sig_Apop[logical_1,]
sig_Apop_FPKM <- sig_Apop_FPKM[logical_1.5,]
sig_Anti_Apop <- sig_Anti_Apop[logical_2,]
sig_TF <- sig_TF[logical_3,]
sig_Immune <- sig_Immune[logical_4,]
sig_Inflam <- sig_Inflam[logical_5,]

mat_Apop <- as.matrix(sig_Apop)
mat_Apop_FPKM <- as.matrix(sig_Apop_FPKM)
mat_Anti_Apop <- as.matrix(sig_Anti_Apop)
mat_TF <- as.matrix(sig_TF)
mat_Immune <- as.matrix(sig_Immune)
mat_Inflam <- as.matrix(sig_Inflam)

mat_Apop_1 <- mat_Apop[1:129,]
mat_Apop_2 <- mat_Apop[130:259,]
mat_Inflam_1 <- mat_Inflam[0:90,]
mat_Inflam_2 <- mat_Inflam[91:181,]

colnames(mat_Apop) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Apop_FPKM) <- c("WT","PG381")
colnames(mat_Apop_1) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Apop_2) <- c("PG381","DPG3","MFI", "MFB")
colnames(cluster_Apop_2) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Anti_Apop) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_TF) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Immune) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Inflam) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Inflam_1) <- c("PG381","DPG3","MFI", "MFB")
colnames(mat_Inflam_2) <- c("PG381","DPG3","MFI", "MFB")

colors = c(seq(-10,-3,length=5),seq(-2.9,3,length=10),seq(3.1,10,length=5))
colors2 = c(seq(0,20,length=5),seq(21,75,length=5))
my_palette <- colorRampPalette(c("green","grey18","red"))(n = 19)
my_palette2 <- colorRampPalette(c("green","red"))(n=9)
rows_Apop <- nrow(mat_Apop)
rows_Apop_FPKM <- nrow(mat_Apop_FPKM)
rows_Apop_1 <- nrow(mat_Apop_1)
rows_Apop_2 <- nrow(mat_Apop_2)
rows_Anti_Apop <- nrow(mat_Anti_Apop)
rows_TF <- nrow(mat_TF)
rows_Immune <- nrow(mat_Immune)
rows_Inflam <- nrow(mat_Inflam)
rows_Inflam_1 <- nrow(mat_Inflam_1)
rows_Inflam_2 <- nrow(mat_Inflam_2)

col_Apop <- ncol(mat_Apop)
col_Anti_Apop <- ncol(mat_Anti_Apop)
col_TF <- ncol(mat_TF)
col_Immune <- ncol(mat_Immune)
col_Inflam <- ncol(mat_Inflam)

mat_trans_apop <- log2(mat_Apop_FPKM)

pdf("testApopFPKM.pdf", width=4.5, height=7.5)
par(cex.main=.9)
heatmap.2(mat_Apop_FPKM, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:2),rowsep=c(0:rows_Apop_FPKM),sepcolor = "black",sepwidth = c(0.05,rep(0.000005,rows_Apop)),
          breaks=colors,symm=F,symkey=F,symbreaks=F, scale="none",
          key.title="none",key.xlab="FPKM",density.info = "none",
          key.par = list(mar=c(.5,0,.5,8)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,4.4,.2,.4),
          main="Differential expression \n Pro Apoptosis/Cell Death",cexCol = 1.1,margins = c(4,9),
          labRow = ""
)
dev.off()




pdf("testApop.pdf", width=4.5, height=7.5)
par(cex.main=.9)
heatmap.2(mat_Apop, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Apop),sepcolor = "black",sepwidth = c(0.05,rep(0.000005,rows_Apop)),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(.5,0,.5,8)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,4.4,.2,.4),
          main="Differential expression \n Pro Apoptosis/Cell Death",cexCol = 1.1,margins = c(4,9),
          labRow = ""
)
dev.off()

pdf("Updated_RNAseq.pdf", width=4.5, height=7.5)
heatmap.2(mat_Apop_1, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Apop_1),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,1,5.7,2)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,3.4,.98,.37),
          main="Differential Expression \n Pro Apoptosis/Cell Death",cexCol = 1.1,margins = c(4,9)
)
dev.off()

pdf("testApop2.pdf", width=4.5, height=7.5)
heatmap.2(mat_Apop_2, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Apop_2),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,1,5.7,2)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,3.4,.98,.37),
          main="Differential Expression \n Pro Apoptosis/Cell Death",cexCol = 1.1,margins = c(4,9)
)
dev.off()



pdf("testAnti.pdf", width=4.5, height=7.5)
par(cex.main=.9)
heatmap.2(mat_Anti_Apop, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Anti_Apop),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(.5,0,.5,8)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,4.4,.2,.4),
          main="Differential Expression \n Anti Apoptosis/Cell Survival/ \n Proliferation",
          cexCol = 1.1,margins = c(4,9)
)
dev.off()

pdf("testTF.pdf", width=4.5, height=7.5)
par(cex.main=.5)
heatmap.2(mat_TF, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_TF),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(.5,0,.5,8)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,4.4,.2,.4),
          main="Differential Expression \n Tracription Factors",
          cexCol = 1.1,margins = c(4,9)
)
dev.off()

pdf("testImmune.pdf", width=3.7, height=7.5)
par(cex.main=.9)
heatmap.2(mat_Immune, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Immune),sepcolor = "black",sepwidth = c(0.08,0.2),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,0,1.3,8)),
          #keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.15,1),lhei = c(.2,.62,.2,.9),
          main="Differential Expression \n Immunoregulation/ \n Immunosuppression",
          cexCol = 1.1,cexRow = .9,margins = c(4,9)
)
dev.off()

pdf("testInflam.pdf", width=4.5, height=7.5)
par(cex.main=.9)
heatmap.2(mat_Inflam, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Inflam),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(.5,0,.5,8)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,4.4,.2,.4),
          main="Differential Expression \n Inflammatory_Response",
          cexCol = 1.1,margins = c(4,9),
          labRow = ""
)
dev.off()

pdf("testInflam1.pdf", width=4.5, height=7.5)
heatmap.2(mat_Inflam_1, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Inflam_1),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,1,5.7,2)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,3.4,.98,.37),
          main="Differential Expression \n Inflammatory Response",cexCol = 1.1,margins = c(4,9)
)
dev.off()

pdf("testInflam2.pdf", width=4.5, height=7.5)
heatmap.2(mat_Inflam_2, col=my_palette, trace="none",dendrogram = "none",
          colsep = c(0:5),rowsep=c(0:rows_Inflam_2),sepcolor = "black",sepwidth = c(0.05,0.4),
          breaks=colors,symm=F,symkey=F,symbreaks=T, scale="none",
          key.title="none",key.xlab="log2(foldchange)",density.info = "none",
          key.par = list(mar=c(3,1,5.7,2)),keysize = .0002,
          lmat = rbind(c(0,3),c(0,1),c(0,4),c(0,2)),lwid = c(.3,1.61),lhei = c(.5,3.4,.98,.37),
          main="Differential Expression \n Inflammatory Response",cexCol = 1.1,margins = c(4,9)
)
dev.off()


######FPKM fold change
test <- cuff_Apop[,2:6]
vals <- apply(test,2,median)
apply(test,2,function(x) x-vals)



sig_Apop_FPKM <- cuff_Apop[!logical_1,2:6]
sig_Apop_FPKM <- na.omit(sig_Apop_FPKM)
logical_1 <- apply(sig_Apop_FPKM,1, function(x) all(is.finite(x)))
sig_Apop_FPKM <- sig_Apop_FPKM[logical_1,]
mat_Apop_FPKM <- as.matrix(apply(sig_Apop_FPKM,2,function(x) x-vals))

mat_Apop_FPKM[1,2]
sig_Apop_FPKM[1,2]

t <- setdiff(sig_Apop_FPKM,vals)
cuff_Apop[1,3]

